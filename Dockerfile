FROM caddy:alpine
LABEL maintainer="Xbird <contact@xbird.me>"

#update apk
RUN apk update && \
    apk upgrade

#install php
RUN apk add \
    php82 \
    php82-apcu \
    php82-ctype \
    php82-dom \
    php82-fpm \
    php82-iconv \
    php82-intl \
    php82-mbstring \
    php82-opcache \
    php82-openssl \
    php82-pdo \
    php82-pdo_mysql \
    php82-phar \
    php82-posix \
    php82-session \
    php82-simplexml \
    php82-tokenizer \
    php82-xml \
    php82-xmlreader \
    php82-xmlwriter

#create a link to use php in cmdline
RUN ln -s /usr/bin/php82 /usr/bin/php

#configure php-fpm to run with a socket
RUN sed -i 's|listen = 127.0.0.1:9000|listen = /var/run/php-fpm8.sock|g' /etc/php82/php-fpm.d/www.conf

#setup php STDOUT from official php docker
COPY --from=php:8.2-fpm /usr/local/etc/php-fpm.d/docker.conf /etc/php82/php-fpm.d/docker.conf

#Disable php access log
RUN sed -i 's|;access.log = log/php82/$pool.access.log|access.log = /dev/null|g' /etc/php82/php-fpm.d/www.conf

#Set php use root user (caddy start with root  [--allow-to-run-as-root])
RUN sed -i 's|user = nobody|user = root|g' /etc/php82/php-fpm.d/www.conf
RUN sed -i 's|group = nobody|group = root|g' /etc/php82/php-fpm.d/www.conf


#read global env variable
RUN sed -i 's|variables_order = "GPCS"|variables_order = "EGPCS"|g' /etc/php82/php.ini

#configure OPCache for Symfony best performances
#https://symfony.com/doc/current/performance.html
RUN sed -i 's|opcache.memory_consumption=128|opcache.memory_consumption=256|g' /etc/php82/php.ini
RUN sed -i 's|opcache.max_accelerated_files=10000|pcache.max_accelerated_files=20000|g' /etc/php82/php.ini
RUN sed -i 's|opcache.validate_timestamps=1|opcache.validate_timestamps=0|g' /etc/php82/php.ini


#install composer from docker image
COPY --from=composer:latest /usr/bin/composer /usr/local/bin/composer

#copy config files
COPY include/Caddyfile /etc/caddy/Caddyfile
RUN caddy fmt --overwrite /etc/caddy/Caddyfile
COPY --chown=root:root include/start.sh /srv/start.sh
RUN chmod +x /srv/start.sh

#setup workdir
WORKDIR /usr/share/caddy

#clean files
RUN rm -rf /tmp/* /var/tmp/* /root/* /data/composer /var/cache/apk/* /usr/share/caddy/*

#add phpinfo display
COPY include/index.php /usr/share/caddy/public/index.php

#use start.sh as entrypoint
ENTRYPOINT ["/srv/start.sh"]