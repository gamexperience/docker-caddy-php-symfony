# docker-caddy-php



## Welcome !

This repository is about to build the gamexperience/docker-caddy-php-symfony docker image

even if this repository aims to run a symfony web application, you can use it with other php applications


## Working Directory

WORKDIR is '/usr/share/caddy', but website root is '/usr/share/caddy/public' so place your files inside

if your project is a symfony app, just copy files in '/usr/share/caddy' (/public is already in your app)

## Ports

Caddy is configured to only respond on 80 for the php app, and on the 2019 one for caddy admin api.

This is not inteded to be used 'as it', but behind a proxy, like Traefik, so 443 is not configured.

Feel free to edit the include/Caddyfile as you need, I recommend to not allow the 2019 port unless you know what you are doing

## How to use in your own app

build your dockerfile using

FROM gamexperience/docker-caddy-php-symfony

then copy your files into '/usr/share/caddy' (or '/usr/share/caddy/public' depending of your app)